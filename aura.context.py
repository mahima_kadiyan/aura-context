import os
import sys
import ssl
import re
import urllib.request
from urllib.parse import urlparse
from urllib.error import URLError, HTTPError


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'

def get_aura_context(url):
    
    response_body = ''
    try:
        response_body = http_request_execution(url)
    except Exception as e:
        print("[-] Failed to access the url")

    if ("window.location.href ='%s" % url) in response_body:
        location_url = re.search(r'window.location.href =\'([^\']+)', response_body)
        url = location_url.group(1)
        try:
            response_body = http_request_execution(url)
        except Exception as e:
            print("[-] Failed to access the redirect url")

    aura_encoded = re.search(r'\/s\/sfsites\/l\/([^\/]+fwuid[^\/]+)', response_body)
    
    if aura_encoded is not None:
        response_body = urllib.parse.unquote(aura_encoded.group(1))
       
    fwuid = re.search(r'"fwuid":"([^"]+)', response_body)
    markup = re.search(r'"(APPLICATION@markup[^"]+)":"([^"]+)"', response_body)
    app = re.search(r'"app":"([^"]+)', response_body)

    if fwuid is None or markup is None or app is None:
        print("Couldn't find fwuid or markup")

    if fwuid is not None or markup is not None or app is not None:
        aura_context = '{"mode":"PROD","fwuid":"' + fwuid.group(1)
        aura_context += '","app":"' + app.group(1) + '","loaded":{"' + markup.group(1) 
        aura_context += '":"' + markup.group(2) + '"},"dn":[],"globals":{},"uad":false}'
    else:
        aura_context ='{"mode":"PROD","fwuid":"AE898lCB2KpCUerBipCwXg","app":"siteforce:communityApp","loaded":{"APPLICATION@markup://siteforce:communityApp":"lo4GGw-Pmo6GDDtL3RhwPw"},"dn":[],"globals":{},"uad":false}'

    return aura_context

def http_request_execution(url, values='', method='GET'):
    headers = {
        'User-Agent': USER_AGENT
    }
    if method == 'POST':
        headers['Content-Type'] = 'application/x-www-form-urlencoded'
        data = urllib.parse.urlencode(values)
        data = data.encode('ascii')
        request = urllib.request.Request(url, data=data, method=method, headers=headers)
    else:
        request = urllib.request.Request(url, method=method, headers=headers)
        
    response_body = ''
    try:
        with urllib.request.urlopen(request, context=ctx) as response:
            response_body = response.read().decode("utf-8")
    except URLError as e:
        raise
    return response_body

def find_context():
    context = get_aura_context('https://mtxdev-vermont.cs32.force.com/VTBizExpress/s/')
    print('aura.context:',context)
find_context()